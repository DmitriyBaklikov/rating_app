function Controller() {
  this.substitutes = {'Create': 'New', 'Update': 'Edit'};

  var controller_name = this.controller_name = $("body").data("controller");
  var action_name = this.action_name = $("body").data("action");
  if('function' !== typeof(window[controller_name])) {
    console.warn("Controller", controller_name, "is not a function");
    return;
  };

  window[controller_name].apply(this); // run constructor in the scope of current object
  if(!this[action_name]) { // undefined, try to find matching REST action
    if(action_name in this.substitutes)
      action_name = this.substitutes[action_name];
  };
  if('function' !== typeof(this[action_name])) {
    console.warn("Action", action_name, "is not a function in controller", controller_name);
    return;
  }

  this[action_name]();
};
