(($) ->
  count = 0

  $.quantityPicker = (element, options) ->
    $element = $(element)
    plugin = this
    $all = undefined
    $wrap = undefined
    bc = undefined
    bg = undefined
    $bg = undefined
    activeClass = undefined
    closeSpeed = undefined
    defaults =
      trigger: "click"
      closeHTML: "&#215;"
      appearUnder: element
      baseClass: "quantityPicker"
      escape: true
      clickOff: true
      maskOpacity: 0
      animationSpeed: 370

      onTrigger: ->
      onDisplay: ->
      onClose: ->

    plugin.settings = {}

    plugin.init = ->
      plugin.settings = $.extend({}, defaults, options)
      bc = plugin.settings.baseClass
      bg = "." + bc + "-bg"
      $bg = $(bg)
      activeClass = bc + "-active"
      closeSpeed = plugin.settings.animationSpeed / 2.5
      buildHTML()
      setPickerHandler()
      close = "." + bc + "-wrap a." + bc + "-close"
      close += (if (plugin.settings.clickOff) then ", " + bg else "")
      $element.bind plugin.settings.trigger, (e) ->
        window.current_input = $(this)
        plugin.settings.onTrigger.call this
        plugin.showquantityPicker()
        e.preventDefault()

      if plugin.settings.escape
        $("body").keyup (e) ->
          if e.which is 27
            speed = (if (e.shiftKey) then closeSpeed * 7 else closeSpeed)
            plugin.hidequantityPicker speed

      $(close).click (e) ->
        speed = (if (e.shiftKey) then closeSpeed * 7 else closeSpeed)
        plugin.hidequantityPicker speed
        e.preventDefault()

      plugin

    plugin.destroy = ->

    plugin.showquantityPicker = (speed) ->
      speed = (if typeof (speed) isnt "undefined" then speed else plugin.settings.animationSpeed)
      plugin.hidequantityPicker()
      plugin.settings.onDisplay.call this
      $bg.css("opacity", plugin.settings.maskOpacity).show()
      $element.addClass activeClass
      $under = $(plugin.settings.appearUnder)
      parentField = $under.position()
      pos = $under.offset()
      width = $under.width()
      height = $under.height()
      pop = $wrap.width()

      if parentField.left > ($(window).width() / 2) && pos.top > ($(window).height() / 2)
        $wrap.css(
          left: (pos.left + (width / 2) - pop) + "px"
          top: (pos.top - 310) + "px"
        ).fadeIn speed
      else if parentField.left < ($(window).width() / 2) && pos.top > ($(window).height() / 2)
        $wrap.css(
          left: (pos.left) + "px"
          top: (pos.top - 310) + "px"
        ).fadeIn speed
      else if parentField.left > ($(window).width() / 2) && pos.top < ($(window).height() / 2)
        $wrap.css(
          left: (pos.left + (width / 2) - pop) + "px"
          top: (pos.top + height + 14) + "px"
        ).fadeIn speed
      else
        $wrap.css(
          left: (pos.left) + "px"
          top: (pos.top + height + 14) + "px"
        ).fadeIn speed

    plugin.hidequantityPicker = (speed) ->
      speed = (if typeof (speed) isnt "undefined" then speed else closeSpeed)
      plugin.settings.onClose.call this
      $bg.hide()
      $("." + activeClass).removeClass activeClass
      $all.fadeOut speed

    setPickerHandler = ->
      # Quantity Select 
      $wrap.find(".qselector-condition").live "click", ->
        if $(this).find("option:selected").val() == "between"
          $(this).closest("tr").next().show("slow")
        else
          $(this).closest("tr").next().hide("slow")

      # Input digits validation
      $wrap.find(".qs-amount").live "keyup", ->
        val = $(this).val()
        if isNaN(val)
          val = val.replace(/[^0-9\.]/g, "")
          val = val.replace(/\.+$/, "") if val.split(".").length > 2
        $(this).val val

      # Table quantityAmount picker
      $wrap.find(".qselector-submit").live "click", ->
        $input = window.current_input
        quantityAmount = {}
        if $(".qs-second-option").is(":visible")
          quantityAmount["condition"] = $wrap.find(".qselector-condition").val()
          quantityAmount["first_amount"] = $wrap.find(".qselector-amount").val()
          quantityAmount["separator"] = "and"
          quantityAmount["second_amount"] = $wrap.find(".qselector-amount-between").val()
          quantityAmount["measure"] = $wrap.find(".qselector-measurer").val()
        else
          quantityAmount["condition"] = $wrap.find(".qselector-condition").val()
          quantityAmount["first_amount"] = $wrap.find(".qselector-amount").val()
          delete quantityAmount["second_amount"]
          quantityAmount["measure"] = $wrap.find(".qselector-measurer").val()
        if quantityAmount["first_amount"] == ''
          $(".qselector-amount").addClass("qs-no-input")
        else
          if quantityAmount["Second amount"] == ''
            $(".qselector-amount-between").addClass("qs-no-input")
          else
            $(".qselector-amount").removeClass("qs-no-input")
            quantityAmountRow = []
            $.each quantityAmount, (key, value) ->
              quantityAmountRow += value + " "
            $input.prop('value', quantityAmountRow["0"].toUpperCase() + quantityAmountRow.slice(1).slice(0,-1)).change()
            plugin.hidequantityPicker()
        
    buildHTML = ->
      # Variables set for selects
      conditionsList  = ["exactly", "max", "min", "between"]
      measurersList   = ["%", "EUR", "USD", "UAH"]

      $("body").append "<div class=\"" + bc + "-bg\" />"  unless $bg.length
      $bg = $(bg)
      markup =  "<div class=\"" + bc + "-wrap\"><div class=\"" + bc + "\">"
      markup += "<a href=\"#\" class=\"" + bc + "-close\">" + plugin.settings.closeHTML + "</a>"
      markup += "<table class='qselector'><thead><th colspan='3'>Quantity Selector</th></thead>
                 <tbody><tr class='qs-first-option'><td><select class='qselector-condition'>"
      # Conditions select
      $.each conditionsList, (key, value) ->
        markup += "<option value=\"" + value + "\">" + value + "</option>"            
      markup += "</td><td><input type='text' size='40' class='qselector-amount qs-amount'></td><td>
                <select class='qselector-measurer'>"
      # Meaasurers select
      $.each measurersList, (key, value) ->
        markup += "<option value=\"" + value + "\">" + value + "</option>"
      markup += "</select></td></tr><tr class='qs-second-option'>
                 <td>And</td><td><input type='text' size='40' class='qselector-amount-between qs-amount'></td>
                 <td></td></tr></tbody></table><button class='btn btn-success qselector-submit'>Update</button>"
      # end
      markup += "<div class=\"clear\" />"
      $("body").append markup
      $all = $("." + bc + "-wrap")
      $wrap = $($all[count])

    plugin.init()

  $.fn.quantityPicker = (options) ->
    @each ->
      $this = $(this)
      return if $this.data("quantityPicker")
      plugin = new $.quantityPicker(this, options)
      $this.data "quantityPicker", plugin
      count++

) jQuery