#encoding: utf-8
class CreditRatingsController < InheritedResources::Base
  before_filter :authenticate_user!

  def show
    if request.xhr?
      @credit_rating = CreditRating.find(params[:id])
      render layout: false
    else
      show!
    end
  end
end
