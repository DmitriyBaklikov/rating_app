module ApplicationHelper
  def body_opts
    {class: [controller_name, action_name], data: {controller: controller_name.capitalize, action: action_name.capitalize}}
  end
end
