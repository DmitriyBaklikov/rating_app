class CreateCreditRatings < ActiveRecord::Migration
  def change
    create_table :credit_ratings do |t|
      t.string :moodys_lt
      t.string :moodys_st
      t.string :sp_lt
      t.string :sp_st
      t.string :fitch_lt
      t.string :fitch_st
      t.string :name

      t.timestamps
    end
  end
end
