puts 'CREATING ROLES'
Role.create([
  { :name => 'admin' }, 
  { :name => 'user' }
], :without_protection => true)

puts 'SETTING UP DEFAULT USER LOGIN'
user = User.create! :name => 'First User', :email => 'user@example.com', :password => 'please', :password_confirmation => 'please'
puts 'New user created: ' << user.name
user.add_role :admin

puts "CREATING RATINGS"
File.open("#{Rails.root}/db/ratings.csv") do |ratings|
  ratings.read.each_line do |rating|
    moodys_lt, moodys_st, sp_lt, sp_st, fitch_lt, fitch_st, name = rating.chomp.split(",")
    CreditRating.create!(
        moodys_lt: moodys_lt,
        moodys_st: moodys_st,
        sp_lt: sp_lt,
        sp_st: sp_st, 
        fitch_lt: fitch_lt, 
        fitch_st: fitch_st, 
        name: name)
  end
end