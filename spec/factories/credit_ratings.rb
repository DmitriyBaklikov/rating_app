# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :credit_rating do
    moodys_lt "MyString"
    moodys_st "MyString"
    sp_lt "MyString"
    sp_st "MyString"
    fitch_lt "MyString"
    fitch_st "MyString"
    name "MyString"
  end
end
